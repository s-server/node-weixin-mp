var util = require('util');
var express = require('express');
var events = require('events');
var xmlBodyParser = require('../middleware/xml-body-parser');

module.exports = WeixinApiServer;

function WeixinApiServer() {
  var self = this;

  self.app = express();

  /**
   * 使用xmlBodyParser
   */
  self.app.use(xmlBodyParser);


  self.app.get('/', function(req, res) {
    /**
     * 微信服务器向我们的接口以GET方式进行微信开发者验证
     */
    if (req.query.signature) {
      self.emit('verify', req, res);

      return;
    }

    res.status(200).send('GET /');
  });

  self.app.post('/', function(req, res) {
    /**
     * 微信服务器向我们的接口以POST方式发送消息
     */
    if (req.method === 'POST') {
      self.emit('message', req, res);

      return;
    }

    res.status(200).send('POST /');
  });

  /**
   * 其他的所有请求都返回404
   */
  self.app.use(function(req, res, next){
    res.status(404).send('Sorry, cant find that!');
  });
}

/**
 * 继承事件原型
 */
util.inherits(WeixinApiServer, events.EventEmitter);

WeixinApiServer.prototype.listen = function (port) {
  this.app.listen(port);
  console.log("Listen on " + port);
}
