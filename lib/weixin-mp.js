var util = require('util');
var events = require('events');
var request = require('request');
var weixinPublicStationAPI = require('weixin-public-station-api');
var http = require('http');
var WeixinApiServer = require('./weixin-api-server');
var TextMessage = require('./text-message');
var crypto = require('crypto');

module.exports = WeixinMP;

/**
 * 导出文本消息
 */
exports.TextMessage = TextMessage;

/**
 * 微信公众平台类
 * @author shallker
 */
function WeixinMP (config) {
  var self = this;

  /**
   * 验证传参
   */
  if (arguments.length === 0) {
    throw new Error('[error] missing argument cofnig');
  }

  /**
   * 验证配置
   * 配置 token 必需
   */
  if (!config.token) {
    throw new Error('[error] config token is required');
  }

  /**
   * 配置 appid 必需
   */
  if (!config.appid) {
    throw new Error('[error] config appid is required');
  }

  /**
   * 配置 appsecret 必需
   */
  if (!config.appsecret) {
    throw new Error('[error] config appsecret is required');
  }

  /**
   * 配置
   */
  self.config = {
    token: config.token,
    appid: config.appid,
    appsecret: config.appsecret,
    url: config.url
  }

  /**
   * 配置数据缓存对象
   */
  self.cache = {}

  /**
   * 创建微信接口服务器
   */
  self.weixinApiServer = new WeixinApiServer();

  /**
   * 收到微信向我们发来的开发者验证
   */
  self.weixinApiServer.on('verify', function (req, res) {
    var str = [self.config.token, req.query.timestamp, req.query.nonce].sort().join('');
    var sha1 = crypto.createHash('sha1').update(str).digest("hex");

    if (sha1 == req.query.signature) {
      res.status(200).send(req.query.echostr);

      /**
       * 触发验证成功事件
       */
      self.emit('verified');
    } else {
      res.status(400).send('false');
    }
  });

  /**
   * 收到微信向我们发来的消息
   */
  self.weixinApiServer.on('message', function (req, res) {
    self.emit('message', req.body.xml);

    /**
     * 文字消息
     */
    if (req.body.xml.MsgType[0] === 'text') {
      var textMessage = new TextMessage();

      textMessage.parseXML(req.body.xml);

      self.emit('text-message', textMessage, function (replyMessage) {
        res.status(200).send(replyMessage.toXML());
      });
    }
  });
}

/**
 * 继承事件
 */
util.inherits(WeixinMP, events.EventEmitter);

/**
 * 设置监听端口
 */
WeixinMP.prototype.listen = function (port) {
  var self = this;

  /**
   * 验证参数
   */
  if (arguments.length === 0) {
    throw new Error('[error] mising argument port');
  }

  if (typeof port !== 'number') {
    throw new Error('[error] wrong argument type');
  }

  /**
   * 监听端口
   */
  self.weixinApiServer.listen(port);
}

/**
 * 获取 access token
 * @param  {Function} callback 回调函数
 */
WeixinMP.prototype.getAccessToken = function (callback) {
  var self = this;

  /**
   * 如果缓存中已经存在access token，则返回缓存中的access token
   */
  if (self.cache['access-token']) {
    callback(self.cache['access-token']);

    return;
  }

  /**
   * 对微信获取access token的接口发起请求
   */
  weixinPublicStationAPI.requestAccessToken(self.config.appid, self.config.appsecret, function (response) {
    /**
     * 将获取到的access token缓存起来
     */
    self.cache['access-token'] = response.access_token;

    /**
     * 将获取到的access token返回给回调函数
     */
    callback(self.cache['access-token']);
  });
}

WeixinMP.prototype.setCustomMenu = function (menus, callback) {
  var self = this;

  /**
   * 获取access token
   */
  self.getAccessToken(function (accessToken) {
    weixinPublicStationAPI.createCustomMenu(accessToken, menus, function (response) {
      if (response.errcode === 0) {
        callback && callback(true);

        return;
      }

      callback && callback(false);
    });
  });
}

function weixinTextMessage(messageContent) {
  var xml = this;

  var text = "";
      text += "<xml>";
      text += "<ToUserName><![CDATA[" + xml.FromUserName[0] + "]]></ToUserName>";
      text += "<FromUserName><![CDATA[" + xml.ToUserName[0] + "]]></FromUserName>";
      text += "<CreateTime>" + xml.CreateTime[0] + "</CreateTime>";
      text += "<MsgType><![CDATA[text]]></MsgType>";
      text += "<Content><![CDATA[" + messageContent + "]]></Content>";
      text += "</xml>";

  return text;
}
