module.exports = TextMessage;

/**
 * 微信的XML格式消息
 */
function TextMessage() {
  var self = this;

  /**
   * 配置
   */
  self.config = {};
}

/**
 * 设置消息发送者
 */
TextMessage.prototype.setMessageFrom = function (from) {
  this.messageFrom = from;
}

/**
 * 获取消息发送者
 */
TextMessage.prototype.getMessageFrom = function () {
  return this.messageFrom;
}

/**
 * 设置消息接受者
 */
TextMessage.prototype.setMessageTo = function (to) {
  this.messageTo = to;
}

/**
 * 获取消息接受者
 */
TextMessage.prototype.getMessageTo = function () {
  return this.messageTo;
}

/**
 * 设置消息时间
 */
TextMessage.prototype.setMessageTime = function (time) {
  this.messageTime = time;
}

/**
 * 获取消息时间
 */
TextMessage.prototype.getMessageTime = function (time) {
  return this.messageTime;
}

/**
 * 设置消息文本
 */
TextMessage.prototype.setMessageContent = function (content) {
  this.messageContent = content;
}

/**
 * 获取消息文本
 */
TextMessage.prototype.getMessageContent = function () {
  return this.messageContent;
}

TextMessage.prototype.getMessageObject = function () {
  return {
    messageTo: this.messageTo,
    messageFrom: this.messageFrom,
    messageTime: this.messageTime,
    messageContent: this.messageContent
  }
}

/**
 * 获取XML格式的消息
 */
TextMessage.prototype.toXML = function () {
  var xml = "";

  xml += "<xml>";
  xml += "<ToUserName><![CDATA[" + this.messageTo + "]]></ToUserName>";
  xml += "<FromUserName><![CDATA[" + this.messageFrom + "]]></FromUserName>";
  xml += "<CreateTime>" + this.messageTime + "</CreateTime>";
  xml += "<MsgType><![CDATA[text]]></MsgType>";
  xml += "<Content><![CDATA[" + this.messageContent + "]]></Content>";
  xml += "</xml>";

  return xml;
}

TextMessage.prototype.parseXML = function (xml) {
  this.messageFrom = xml.FromUserName[0];
  this.messageTo = xml.ToUserName[0];
  this.messageType = xml.MsgType[0];
  this.messageContent = xml.Content[0];
  this.messageId = xml.MsgId[0];
  this.messageTime = xml.CreateTime[0];

  /**
   * 设置消息类型
   */
  this.type = this.messageType;
}
