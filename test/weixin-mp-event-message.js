var assert = require("assert");
var WeixinMP = require('../lib/weixin-mp');
var weixinConfig = require('../data/weixin-config');

var weixinmp = new WeixinMP(weixinConfig);

weixinmp.listen(8881);

weixinmp.on('message', function (a, b) {
  console.log('message');
  console.log('a', a);
  console.log('b', b);
});
