var assert = require("assert");
var WeixinMP = require('../lib/weixin-mp');
var weixinConfig = require('../data/weixin-config');

describe('WeixinMP', function() {
  describe('method: getAccessToken(callback)', function() {
    var weixinmp = new WeixinMP(weixinConfig);

    it('should exists', function() {
      assert.equal(typeof weixinmp.getAccessToken, 'function');
    });
  });

  describe('method: listen(port)', function() {
    var weixinmp = new WeixinMP(weixinConfig);

    it('should exists', function() {
      assert.equal(typeof weixinmp.listen, 'function');
    });

    it('should listen on port', function(done) {
      weixinmp.listen(8784);
    });
  });
});
