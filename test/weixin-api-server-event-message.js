var assert = require("assert");
var WeixinApiServer = require('../lib/weixin-api-server');

var weixinApiServer = new WeixinApiServer();

weixinApiServer.listen(8881);

weixinApiServer.on('message', function (a, b) {
  console.log('message');
  console.log('a', a);
  console.log('b', b);
});

weixinApiServer.on('verify', function (a, b) {
  console.log('verify');
  console.log('a', a);
  console.log('b', b);
});
